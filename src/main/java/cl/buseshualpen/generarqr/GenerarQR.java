package cl.buseshualpen.generarqr;

import cl.buseshualpen.generarqr.gui.FormGenerar;

import javax.swing.*;

public class GenerarQR {

    public static void main(String[] arg) {
        JFrame frame = new FormGenerar("GeneradorQR");
        frame.setSize(600, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
