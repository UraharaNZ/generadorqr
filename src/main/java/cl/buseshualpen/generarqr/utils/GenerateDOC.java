package cl.buseshualpen.generarqr.utils;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;

public class GenerateDOC {

    public static void createDOC(String pathFolderQR, String pathFolderPDF) {
        try {
            File folder = new File(pathFolderQR);
            File[] folderFiles = folder.listFiles();

            for (File file : folderFiles) {
                System.out.println(file.getAbsolutePath());
                XWPFDocument docx = new XWPFDocument();
                CTSectPr sectPr = docx.getDocument().getBody().addNewSectPr();
                CTPageMar pageMargins = sectPr.addNewPgMar();
                pageMargins.setLeft(BigInteger.valueOf(500L));
                pageMargins.setRight(BigInteger.valueOf(300L));

                XWPFParagraph par = docx.createParagraph();
                XWPFRun run = par.createRun();

                File folderQR = new File(file.getAbsolutePath());
                File[] imageQR = folderQR.listFiles();
                Arrays.sort(imageQR, Comparator.comparingLong(File::lastModified));

                for (File image : imageQR) {
                    if (image.isFile()) {
                        InputStream pic = new FileInputStream(image.getAbsolutePath());
                        run.addPicture(pic, Document.PICTURE_TYPE_PNG, image.getName(), Units.toEMU(348/1.33), Units.toEMU(200/1.33));
                        run.setText("               ");
                        pic.close();
                    }
                }

                String finalDestination = pathFolderPDF + "\\" + file.getName() + ".doc";
                FileOutputStream out = new FileOutputStream(finalDestination);

                docx.write(out);
                out.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
