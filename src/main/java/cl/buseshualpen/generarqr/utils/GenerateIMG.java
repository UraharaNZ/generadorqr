package cl.buseshualpen.generarqr.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Map;
import java.util.Random;

public class GenerateIMG {

    public static void GenerateQR(String path, String vehiculo, String asiento, String patente, String empresa, String contrato, boolean cleanDirectory) {
        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, 0); /* default = 4 */
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            if (cleanDirectory) {
                cleanDirectory(path);
            }
            initDirectory(path);

            // Load Format
            BufferedImage format = getOverly();

            BufferedImage combined = new BufferedImage(format.getWidth(), format.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics2D = (Graphics2D) combined.getGraphics();
            graphics2D.drawImage(format, 0, 0, null);

            // Generate QR Code
            String md5 = MD5("H!U#A$L%P&E@N" + ":" + vehiculo + ":" + asiento + ":" + patente + ":" + empresa + ":" + contrato);
            bitMatrix = writer.encode("https://qr.buseshualpen.cl/registro/loadCookies/"  + vehiculo + "/" + asiento + "/" + patente + "/" + empresa + "/" + contrato + "/" + md5, BarcodeFormat.QR_CODE, 130, 130, hints);

            // Load QR
            BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, getMatrixConfig());
            graphics2D.drawImage(qrImage, 27, 46, null);

            graphics2D.setColor(Color.black);
            graphics2D.setFont(new Font("Verdana", Font.BOLD, 18));
            graphics2D.drawString("Bus: " + vehiculo, 190, 112);
            if (Integer.parseInt(asiento) < 10) {
                graphics2D.drawString("Asiento: " + asiento, 195, 129);
            } else {
                graphics2D.drawString("Asiento: " + asiento, 189, 129);
            }

            ImageIO.write(combined, "png", outputStream);
            File directory = new File(path + "\\" + vehiculo + "\\");
            if (!directory.exists()) {
                directory.mkdir();
            }

            Files.copy( new ByteArrayInputStream(outputStream.toByteArray()), Paths.get(path + "\\"  + vehiculo + "\\" + asiento + ".png"), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static BufferedImage getOverly() {
        try {
            String fileName = "images/formato.png";
            return ImageIO.read(getFileFromResourceAsStream(fileName));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private static InputStream getFileFromResourceAsStream(String fileName) {
        ClassLoader classLoader = GenerateIMG.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }

    }

    private static void initDirectory(String DIR) {
        try {
            Files.createDirectories(Paths.get(DIR));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private static void cleanDirectory(String DIR) {
        try {
            Files.walk(Paths.get(DIR), FileVisitOption.FOLLOW_LINKS).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private static MatrixToImageConfig getMatrixConfig() {
        return new MatrixToImageConfig(Colors.BLACK.getArgb(), Colors.WHITE.getArgb());
    }

    private static String generateRandomTitle(Random random, int length) {
        return random.ints(48, 122).filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                .mapToObj(i -> (char) i).limit(length).collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }

    private static String MD5(String message) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] array = messageDigest.digest(message.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
